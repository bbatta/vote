#ifndef  MAKEVOTE_H_INCLUDED
#define  MAKEVOTE_H_INCLUDED

#include <string>
class MakeVote
{
	int voteCount;
	std::string voteTitle;
	struct questionStruct
	{
		std::string title;
		int vCount;
	};
	questionStruct *qArr;

public:
	void setCount(int index){
		voteCount = index;
		qArr = new questionStruct[index];
		for (int i = 0; i < index; i++){
			qArr[i].title = "";
			qArr[i].vCount = 0;
		}
	}

	void setArray(int index, std::string str){
		qArr[index].title = str;
	}

	void setCountArray(int index, int vCount){
		if (0 <= index && index < voteCount){
			qArr[index].vCount += vCount;
		}
	}

	int getCount(){
		return voteCount;
	}

	void setVoteTitle(std::string title){
		voteTitle = title;
	}

	std::string getVoteTitle(){
		return voteTitle;
	}

	void qDisplay();
	void resultDisplay();
};

#endif