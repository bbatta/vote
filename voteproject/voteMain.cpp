#include <iostream>
#include <string>
#include "MakeVote.h"
using namespace std;

int main(){

	//title -> 저장될 투표 주제, vcount -> 저장될 답변 갯수
	std::string title;
	int vcount;

	//투표의 주제와 답변의 갯수를 설정
	cout << "투표의 주제를 정해주세요 : ";
	getline(cin, title);
	cout << endl;
	cout << "답변의 갯수를 정해주세요 : ";
	cin >> vcount;
	cout << endl << endl;;
	
	//MakeVote의 맴버변수에 저장한다.
	MakeVote vote;
	vote.setVoteTitle(title);
	vote.setCount(vcount);
	
	//답변을 세팅
	//버퍼 삭제
	cin.clear();
	cin.ignore(INT_MAX, '\n');

	int i = 0;
	while (i < vote.getCount()){ // 설정한 투표 답변의 갯수보다 i가 커질때까지 답변을 생성한다.
		std::string tmp;
		cout << "답변" << (i + 1) << " => ";
		getline(cin, tmp);
		vote.setArray(i, tmp);
		i++;
	}

	cout << endl << endl;
	
	system("PAUSE");
	system("cls");

	//설문 시작
	vote.qDisplay();

	//투표되는 임시값
	int tmp = -1;
	while (tmp != 0){
		if (tmp > vote.getCount()){
			system("cls");
			vote.qDisplay();
			cout << "잘못된 입력값 입니다." << endl;
			cout << "선택(0은 종료) : ";
			cin >> tmp;
		}else{
			system("cls");
			vote.qDisplay();
			cout << "선택(0은 종료) : ";
			cin >> tmp;
		}
		vote.setCountArray(tmp - 1, 1);
	}
	cout << endl;
	
	//설문 결과
	vote.resultDisplay();

	system("PAUSE");
	return 0;
}