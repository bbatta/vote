#include <iostream>
#include <string>
#include "MakeVote.h"
using namespace std;

void MakeVote::qDisplay(){
	cout << endl << voteTitle << endl << endl;
	int i = 0;
	while (i<voteCount){
		cout << "[" << i + 1 << "] " << qArr[i].title << endl;
		i++;
	}

	cout << endl;
}


void MakeVote::resultDisplay(){
	cout << "집계결과입니다." << endl << endl;
	int i = 0;
	while (i < voteCount){
		cout << "[" << i + 1 << "] " << qArr[i].title << " ------> " << qArr[i].vCount << endl;
		i++;
	}
	cout << endl;

	delete[] qArr;
}